# Rachel J. Morris 		2013-07-24		WTFPL CopyFree license http://copyfree.org/licenses/wtfpl2/license.txt

import pygame
import random

class Ero:
	def __init__( self, dimensioj, bildo ):
		self.Kreu( dimensioj, bildo )
	
	def Kreu( self, dimensioj, bildo ):
		self.GeneruKoordinatoj()
		
		self.m_largxo 		= dimensioj[0]
		self.m_alteco		= dimensioj[1]
		
		self.m_bildo 		= bildo
		
		self.m_nomo 		= "Diamanto"
	# Kreu
	
	def PrenuRekt( self ):
		rektangulo = {}
		rektangulo["x"] 		= self.m_x
		rektangulo["y"] 		= self.m_y
		rektangulo["width"] 	= self.m_largxo
		rektangulo["height"]	= self.m_alteco
		
		return rektangulo
	# PrenuRekt
	
	def GeneruKoordinatoj( self ):
		self.m_x = random.randint( 0, 640-32 )
		self.m_y = random.randint( 0, 480-48 )
	# GenerateCoordiantes
	
	def Vidigu( self, fenestro, fontObj ):
		subRekt = self.m_bildo.get_rect()
		subRekt.x = 0
		subRekt.y = 0
		subRekt.width = self.m_largxo
		subRekt.height = self.m_alteco
		
		fenestro.blit( self.m_bildo, ( self.m_x, self.m_y ), subRekt )
		
		teksto = fontObj.render( self.m_nomo, False, pygame.Color( 255, 255, 255 ) )
		tekstoRekt = teksto.get_rect()
		
		tekstoRekt.x = self.m_x - len( self.m_nomo ) * 7 / 2 
		tekstoRekt.y = self.m_y - 28
		fenestro.blit( teksto, tekstoRekt )
	# Vidigu
# Ludanto
