# Rachel J. Morris 		2013-07-24		WTFPL CopyFree license http://copyfree.org/licenses/wtfpl2/license.txt

import pygame

class Ludanto:
	def __init__( self, coords, dimensioj, speed, bildo ):
		self.Kreu( coords, dimensioj, speed, bildo )
	
	def Kreu( self, coords, dimensioj, speed, bildo ):
		self.m_x 			= coords[0]
		self.m_y 			= coords[1]
		
		self.m_largxo 		= dimensioj[0]
		self.m_alteco		= dimensioj[1]
		
		self.m_rapido 		= speed
		self.m_bildo 		= bildo
		
		self.m_direkto 		= 0
		self.m_frame		= 0
		
		self.m_cxuKuras		= False
		self.m_kurasRapido	= 4
		self.m_marsxasRapido	= 2
		
		self.m_nomo 		= "Virino"
		
		self.m_poentaro 		= 0
	# Kreu
	
	def PrenuRekt( self ):
		rektangulo = {}
		rektangulo["x"] 		= self.m_x
		rektangulo["y"] 		= self.m_y
		rektangulo["width"] 	= self.m_largxo
		rektangulo["height"]	= self.m_alteco
		
		return rektangulo
	# PrenuRekt
	
	def IncrementScore( self ):
		self.m_poentaro += 1
	# IncrementScore
	
	def Move( self, klavoj ):
		cxuMovis = False
		
		if 		( klavoj[ pygame.K_LSHIFT ] ):
			self.m_rapido = self.m_kurasRapido
		else:
			self.m_rapido = self.m_marsxasRapido
		
		if 		( klavoj[ pygame.K_LEFT ] ):
			self.MovuMaldekstra()
			cxuMovis = True
			
		elif 	( klavoj[ pygame.K_RIGHT ] ):
			self.MovuDekstra()
			cxuMovis = True
			
		if 		( klavoj[ pygame.K_UP ] ):
			self.MovuSupren()
			cxuMovis = True
			
		elif 	( klavoj[ pygame.K_DOWN ] ):
			self.MovuSuben()
			cxuMovis = True
		
		# Animate
		if ( cxuMovis ):
			self.m_frame += 0.1
			if ( self.m_frame >= 4 ):
				self.m_frame = 0
	# Move
	
	def MovuSupren( self ):
		if ( self.m_y - self.m_rapido >= 0 ):
			self.m_y -= self.m_rapido
			self.m_direkto = 1
	# MovuSupren
	
	def MovuSuben( self ):
		if ( self.m_y + self.m_rapido <= 480-48 ):
			self.m_y += self.m_rapido
			self.m_direkto = 0
	# MovuSuben
	
	def MovuMaldekstra( self ):
		if ( self.m_x - self.m_rapido >= 0 ):
			self.m_x -= self.m_rapido
			self.m_direkto = 2
	# MovuMaldekstra
	
	def MovuDekstra( self ):
		if ( self.m_x + self.m_rapido <= 640-32 ):
			self.m_x += self.m_rapido
			self.m_direkto = 3
	# MovuDekstra
	
	def Vidigu( self, fenestro, fontObj ):
		subRekt = self.m_bildo.get_rect()
		subRekt.x = int( self.m_frame ) * self.m_largxo
		subRekt.y = self.m_direkto * self.m_alteco
		subRekt.width = self.m_largxo
		subRekt.height = self.m_alteco
		
		fenestro.blit( self.m_bildo, ( self.m_x, self.m_y ), subRekt )
		
		nomoKajPoentaro = self.m_nomo + " (" + str( self.m_poentaro ) + ")"
		teksto = fontObj.render( nomoKajPoentaro, False, pygame.Color( 255, 255, 255 ) )
		tekstoRekt = teksto.get_rect()
		
		tekstoRekt.x = self.m_x - len( nomoKajPoentaro ) * 6 / 2 
		tekstoRekt.y = self.m_y - 28
		fenestro.blit( teksto, tekstoRekt )
	# Vidigu
# Ludanto
