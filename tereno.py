# Rachel J. Morris 		2013-07-24		WTFPL CopyFree license http://copyfree.org/licenses/wtfpl2/license.txt

class Tereno:
	def __init__( self, bildo ):
		self.Kreu( bildo )
	# __init__
		
	def Kreu( self, bildo ):
		self.m_bildo = bildo
	# Kreu
	
	def Vidigu( self, fenestro ):
		tileSize = 64
		
		for y in range( 0, 480/64 + 1 ):
			for x in range( 0, 640/64 + 1 ):
				subRekt = self.m_bildo.get_rect()
				subRekt.x 		= 0
				subRekt.y 		= 0
				subRekt.width 	= tileSize
				subRekt.height 	= tileSize
				
				fenestro.blit( self.m_bildo, ( x * tileSize, y * tileSize ), subRekt )
			# x
		# y
		
	# Vidigu
# Tereno
