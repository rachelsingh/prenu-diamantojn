# Rachel J. Morris 		2013-07-24		WTFPL CopyFree license http://copyfree.org/licenses/wtfpl2/license.txt

import pygame
import sys

from ludanto 	import Ludanto
from tereno 	import Tereno
from ero 		import Ero

def CxuKolizio( rekt1, rekt2 ):
	return ( rekt1["x"]						< 		rekt2["x"] + rekt2["width"] 	and
			 rekt1["x"] + rekt1["width"]	>		rekt2["x"] 						and
			 rekt1["y"]						<		rekt2["y"] + rekt2["height"] 	and
			 rekt1["y"] + rekt1["height"] 	>		rekt2["y"] )
# CxuKolizio


pygame.init()
fenestro = pygame.display.set_mode( ( 640, 480 ) )

bildoTereno 		= pygame.image.load( "graphics/Terrain.png" )
bildoLudanto 		= pygame.image.load( "graphics/Ayne.png" )
bildoDiamanto 		= pygame.image.load( "graphics/Gems.png" )

ludanto = Ludanto( (640/2, 480/2), (32, 48), 2, bildoLudanto )
tereno = Tereno( bildoTereno )
diamanto = Ero( (32, 32), bildoDiamanto )

fontObj = pygame.font.Font( "fonts/Averia-Bold.ttf", 24 )

fpsTempo = pygame.time.Clock()

while True:
	for okazo in pygame.event.get():
		if ( okazo.type == pygame.QUIT ):
			pygame.quit()
			sys.exit()
	# for event
	
	klavoj = pygame.key.get_pressed()
	ludanto.Move( klavoj )
	
	if ( CxuKolizio( ludanto.PrenuRekt(), diamanto.PrenuRekt() ) ):
		ludanto.IncrementScore()
		diamanto.GeneruKoordinatoj()
	
	pygame.display.update()
	fpsTempo.tick( 60 )
	
	# Vidigu
	fenestro.fill( pygame.Color( 100, 255, 100 ) )
	tereno.Vidigu( fenestro )
	diamanto.Vidigu( fenestro, fontObj )
	ludanto.Vidigu( fenestro, fontObj )
	
# while True
